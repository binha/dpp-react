import React, { Component } from 'react';
import './Container.css';

export default class MainContainer extends Component {
    render() {
        return (
            <div className="main-container">
                {this.props.children}
            </div>
        );
    }
}

export class SideBar extends Component {
    render() {
        return (
            <div className="container side-bar">
                {this.props.children}
            </div>
        );
    }
}

export class Container extends Component {
    render() {
        return (
            <div className="container">
                {this.props.children}
            </div>
        );
    }
}