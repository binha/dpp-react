import React, { Component } from 'react';

import './VideoPlayer.css';

const BASE_EMBED_URL = 'https://www.youtube.com/embed/';

export class VideoPlayer extends Component {
  render() {
    const embedUrl = `${BASE_EMBED_URL}${this.props.id}`;

    return (
      <div className="video-container">
        <iframe className='video-player' src={embedUrl} frameBorder='0'
          allow='autoplay; encrypted-media' allowFullScreen title='video' />
        <button className="voting-button" alt="that's crap"><i class="fas fa-poop"></i></button>
        <button className="voting-button" alt="that's awesome"><i class="fas fa-star"></i></button>
      </div>
    );
  }

}
export default VideoPlayer;