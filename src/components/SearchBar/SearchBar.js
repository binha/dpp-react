import React, { Component } from 'react';

import './SearchBar.css';

export default class SearchBar extends Component {
    render() {
        return (
            <div className="search-container">
                <form>
                    <label className='search-title' for='search-input'>search video:</label>
                    <div class='search-input-wrapper'>
                        <input id='search-input' className='search-input' type='text' />
                        <button className="search-button">go</button>
                    </div>

                </form>
            </div>

        );
    }
}