import React, { Component } from 'react';
import './Snippet.css';

export default class Snippet extends Component {
    render() {


        return (
            <div className="snippet-container">
                <h3 className='snippet-title'>{this.props.title}</h3>
                <img alt="a video snippet" src={this.props.img} />
            </div>
        );
    }
}