import React, { Component } from 'react';
import Snippet from '../Snippet/Snippet.js';
import * as videos from '../../data/playlist';

import './PlaylistBar.css';

export default class PlaylistBar extends Component {

    render() {
        this.videos = videos['default']['items'];
        console.log(this.videos);

        return (
            <div className="playlist-container">
                <h2>playing next...</h2>
                {this.videos.map(
                    x => {
                        // console.log(x);
                        return <Snippet
                            key={x.etag}
                            img={x.snippet.thumbnails.standard.url}
                            title={x.snippet.title} />
                    }
                )}
            </div>
        );
    }
}