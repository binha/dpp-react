import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {
    render() {
        return (
            <header>
                <div className="title-container">
                    <div className="title-dpp">DPP</div>
                    <div className="subtitle-dpp">Democratic Party Playlist</div>
                </div>
            </header>
        );
    }
}