import React, { Component, Fragment } from 'react';

import './App.css';

import MainContainer, { Container } from './components/Container/Container';
import VideoPlayer from './components/VideoPlayer/VideoPlayer';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import PlaylistBar from './components/PlaylistBar/PlaylistBar';
import SearchBar from './components/SearchBar/SearchBar';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Container>
          <MainContainer>
            <VideoPlayer id='Plcyi5KvCl8' />
          </MainContainer>
          <Sidebar>
            <SearchBar />
            <PlaylistBar />
          </Sidebar>
        </Container>
      </Fragment>
    );
  }
}

export default App;