export default {
    "kind": "youtube#searchListResponse",
    "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/fx5_tG2WzKy6GtBwQN-0GbVPC7I\"",
    "nextPageToken": "CAUQAA",
    "regionCode": "DE",
    "pageInfo": {
        "totalResults": 1000000,
        "resultsPerPage": 5
    },
    "items": [
        {
            "kind": "youtube#searchResult",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/TXRX6Aud4r_3Vl0I_8WFNhckl8c\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "SB-qEYVdvXA"
            },
            "snippet": {
                "publishedAt": "2018-03-19T02:00:25.000Z",
                "channelId": "UCq5hgY37WAryZCwmehDyCaQ",
                "title": "So many cute kittens videos compilation 2018",
                "description": "DONATE : Paypal: mrbeanwow@gmail.com ▻Subscribe for new video: http://goo.gl/Koxcb0 ▻Fanpage: http://goo.gl/8JZskX ▻Thanks For Watching ! Please ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/SB-qEYVdvXA/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/SB-qEYVdvXA/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/SB-qEYVdvXA/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Funny Animals",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/kTCJY_E4FfFOyEdzR3BKBpkhs34\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "2_w-vOR0TuE"
            },
            "snippet": {
                "publishedAt": "2019-01-28T04:53:33.000Z",
                "channelId": "UC5VvB6tmC4lu_WCx7hE0PZA",
                "title": "Learn How Baby Kittens Grow: 0-8 Weeks!",
                "description": "In this adorable and information-packed video, I'll teach you everything you need to know about how to determine a kitten's age, what developmental milestones ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/2_w-vOR0TuE/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/2_w-vOR0TuE/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/2_w-vOR0TuE/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Kitten Lady",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/NIGKcO1cf8mSJEVHZ2U8Kug2EX8\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "aUWLT02Daoo"
            },
            "snippet": {
                "publishedAt": "2018-12-20T07:07:53.000Z",
                "channelId": "UCq5hgY37WAryZCwmehDyCaQ",
                "title": "Cute is Not Enough - Cute Cats and Kittens Doing Funny Things 2018 #11",
                "description": "Cute is Not Enough - Cute Cats and Kittens Doing Funny Things 2018 #11 ▻Subscribe for new video: http://goo.gl/Koxcb0 ▻Fanpage: http://goo.gl/8JZskX ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/aUWLT02Daoo/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/aUWLT02Daoo/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/aUWLT02Daoo/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Funny Animals",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/PRVo3OvPp-OH5vjBZvzV2bVjWzQ\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "DxLOfvaHoYQ"
            },
            "snippet": {
                "publishedAt": "2019-05-01T17:19:28.000Z",
                "channelId": "UCeL2LSl91k2VccR7XEh5IKg",
                "title": "LIVE:  Tiny kittens rescued from the trash - TinyKittens.com",
                "description": "More about our rescue work: http://TinyKittens.com Ways to help: http://TinyKittens.com/help Five tiny kittens were found in a cardboard box next to a dumpster ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/DxLOfvaHoYQ/default_live.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/DxLOfvaHoYQ/mqdefault_live.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/DxLOfvaHoYQ/hqdefault_live.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "TinyKittens HQ",
                "liveBroadcastContent": "live"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/LpAzfDtuj5OMgW5ptJnFpcWq3oM\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "1vZiorgO5Q8"
            },
            "snippet": {
                "publishedAt": "2019-04-30T22:25:33.000Z",
                "channelId": "UC5VvB6tmC4lu_WCx7hE0PZA",
                "title": "Safely Introducing Kittens (From Different Litters!)",
                "description": "Feline friendship is so important--but when you're rescuing kittens from different litters, you'll want to be sure you're introducing them safely. Watch this video to ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/1vZiorgO5Q8/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/1vZiorgO5Q8/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/1vZiorgO5Q8/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Kitten Lady",
                "liveBroadcastContent": "none"
            }
        }
    ]
};