export default {
    "kind": "youtube#playlistItemListResponse",
    "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/QbXcvz1fQMwwVUZsk4VOPO1mBYk\"",
    "nextPageToken": "CAUQAA",
    "pageInfo": {
        "totalResults": 12,
        "resultsPerPage": 5
    },
    "items": [
        {
            "kind": "youtube#playlistItem",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/OnMS8NsaLqSu_6feMJk1GW6w0Sw\"",
            "id": "UEwtUUxRUXVtWHVySXd6OUlOd3BHSGo4Zy0yWE9TMXBqSy4yODlGNEE0NkRGMEEzMEQy",
            "snippet": {
                "publishedAt": "2019-04-12T09:08:41.000Z",
                "channelId": "UCqMgknVzmr6jiW--D7aMIRQ",
                "title": "Weezer - Say It Ain't So",
                "description": "Music video by Weezer performing Say It Ain't So. (C) 2004 Geffen Records",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/ENXvZ9YRjbo/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/ENXvZ9YRjbo/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/ENXvZ9YRjbo/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/ENXvZ9YRjbo/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/ENXvZ9YRjbo/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Bárbara Borges",
                "playlistId": "PL-QLQQumXurIwz9INwpGHj8g-2XOS1pjK",
                "position": 0,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": "ENXvZ9YRjbo"
                }
            }
        },
        {
            "kind": "youtube#playlistItem",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/uQ0uoSlMgmzNHYH7I734Xt4ImNU\"",
            "id": "UEwtUUxRUXVtWHVySXd6OUlOd3BHSGo4Zy0yWE9TMXBqSy41MjE1MkI0OTQ2QzJGNzNG",
            "snippet": {
                "publishedAt": "2019-04-12T12:32:57.000Z",
                "channelId": "UCqMgknVzmr6jiW--D7aMIRQ",
                "title": "Metronomy - The Look (Music Video)",
                "description": "Metronomy - The Look, taken from their album \"The English Riviera\"\n\nDirected by Lorenzo Fonda.\n\nClick here http://po.st/MetronomyYT and subscribe to Metronomy's channel\n\nFollow Metronomy on Instagram http://instagram.com/metronomy\nFollow Metronomy on Facebook https://www.facebook.com/metronomy/\n\nhttp://www.metronomy.co.uk\n\nCheck out Metronomy's albums:\n\nSummer 08 [2016]\nGet it or stream it http://smarturl.it/Summer08\n\nLove Letters [2014] \nGet it or stream it http://smarturl.it/MetronomyLL\n\nLate Night Tales [2012]\nGet it or stream it http://smarturl.it/MetronomyLNT\n\nThe English Riviera [2011] \nGet it or stream it http://smarturl.it/TheEnglishRiviera\n\nNights Out [2008]\nGet it or stream it http://smarturl.it/MetronomyNightsOut\n\nPip Paine (Pay the £5000 You Owe) [2006]\nGet it or stream it http://smarturl.it/MetronomyPipePaine\n\nBecause Music (http://po.st/because)",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/sFrNsSnk8GM/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/sFrNsSnk8GM/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/sFrNsSnk8GM/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/sFrNsSnk8GM/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/sFrNsSnk8GM/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Bárbara Borges",
                "playlistId": "PL-QLQQumXurIwz9INwpGHj8g-2XOS1pjK",
                "position": 1,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": "sFrNsSnk8GM"
                }
            }
        },
        {
            "kind": "youtube#playlistItem",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/z5aLSRmJzYCUUpk-ccARuXZPxFA\"",
            "id": "UEwtUUxRUXVtWHVySXd6OUlOd3BHSGo4Zy0yWE9TMXBqSy4wOTA3OTZBNzVEMTUzOTMy",
            "snippet": {
                "publishedAt": "2019-05-24T11:05:14.000Z",
                "channelId": "UCqMgknVzmr6jiW--D7aMIRQ",
                "title": "Madonna - Bitch I'm Madonna ft. Nicki Minaj",
                "description": "You're watching the official music video for “Bitch I’m Madonna” from ‘Rebel Heart’. \nBuy/Stream ‘Rebel Heart’: https://Madonna.lnk.to/RebelHeartYTPL\nListen to Madonna’s latest release here: https://Madonna.lnk.to/latest \n\nSubscribe to the Madonna Channel! https://Madonna.lnk.to/YouTubeID \n\nCheck out the Official Madonna YouTube Playlists…\nThe Complete Madonna Videography https://Madonna.lnk.to/Videography \n‘Rebel Heart’ album playlist https://Madonna.lnk.to/RebelHeartYTPL \nLive Performances https://Madonna.lnk.to/LiveYT \nMDNA Skin https://Madonna.lnk.to/MDNAskinYT \n\nHelp Us Give Back…\nRaising Malawi http://www.raisingmalawi.org/\nThe Ray of Light Foundation http://www.rayoflight.org/ \n\nStay in touch with Madonna…\nhttp://madonna.com\nhttp://instagram.com/madonna\nhttp://twitter.com/madonna\nhttp://facebook.com/madonna\nhttp://www.madonna.com/newsletter \n\nLYRICS: \n\nYou're gonna love this\nYou can't touch this\n'Cause I'm a bad bitch\n\nWe hit the elevator right up to the rooftop\nThe bass is pumping, make me wanna screw the top off\nYeah, we'll be drinking and nobody's gonna stop us\nAnd we'll be kissing anybody that's around us\n\nI just wanna have fun tonight\n(Blow up this shit tonight)\nPut me under the flashing light\nOh-oh-oh-oh\nLet me blow up this house tonight\n(Gonna blow up)\n\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nNa-na-na-na-na\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nBitch, I'm Madonna\nBitch, bitch I'm Madonna\nBitch, bitch I'm Madonna\n\nWe're jumping in the pool and swimming with our clothes on\nI poured a beer into my shoe and got my freak on\nThe neighbor's pissed and says he's gonna call the Five-O\nIf they show up then we are gonna give a good show\n\nI just wanna go up tonight\nPull me under the flashing light\nOh-oh-oh-oh\nLet me blow up this house tonight\n(Gon' blow up)\n\nUhh\nBeep-beep, bitch move\n\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nNa-na-na-na-na\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nBitch, I'm Madonna\nBitch, bitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\nBitch, I'm Madonna\n\nWho do you think you are?\nYou can't mess with this lucky star\nOh-oh-oh-oh\nWho do you think you are?\n\nPoured up with my nose up and that rose up in that thinga\nI'm froze up, with my stove up, 'cause he eating like it's his dinner\nI run shit, I don't fall back, cause I'm on track, I'm a sprinter\nI'm bossed up, I got 'em awestruck, it's not a toss up, I'm the winner\nBeep-beep, bitch move, 'fore I bang-bang with that uzz\nThat's Miu Miu on my shoes, ain't got a thing left for me to prove\nIt's that bottle service all night, it's that pop and urban just right\nIt's that \"go hard or go home\" zone, bitch\nI'm Madonna, these hoes know\n\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nNa-na-na-na-na\nWe go hard or we go home\nWe gon' do this all night long\nWe get freaky if you want\nBitch, I'm Madonna\n\nWe do it like this\nYou're gonna love this\nYou can't touch this\n'Cause I'm a bad bitch\nWho do you think you are? ('Cause I'm a bad bitch)\nWho do you think you are? ('Cause I'm a bad bitch)\nWho do you think you are? ('Cause I'm a bad bitch)\nWho do you think you are?\n\nGo hard or go home zone, bitch\nI'm Madonna, these hoes know\n\n\nhttp://vevo.ly/tIakEF",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/7hPMmzKs62w/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/7hPMmzKs62w/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/7hPMmzKs62w/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/7hPMmzKs62w/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/7hPMmzKs62w/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Bárbara Borges",
                "playlistId": "PL-QLQQumXurIwz9INwpGHj8g-2XOS1pjK",
                "position": 2,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": "7hPMmzKs62w"
                }
            }
        },
        {
            "kind": "youtube#playlistItem",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/8jcCml8ZHsLJuY9jq_LWCAPR1EA\"",
            "id": "UEwtUUxRUXVtWHVySXd6OUlOd3BHSGo4Zy0yWE9TMXBqSy4xMkVGQjNCMUM1N0RFNEUx",
            "snippet": {
                "publishedAt": "2019-05-24T11:07:19.000Z",
                "channelId": "UCqMgknVzmr6jiW--D7aMIRQ",
                "title": "Yeah Yeah Yeahs - Y Control",
                "description": "Music video by Yeah Yeah Yeahs performing Y Control. (C) 2003 Interscope Records\n\n#YeahYeahYeahs #YControl #Vevo #Rock #OfficialMusicVideo",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/IcjPFAV1foU/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/IcjPFAV1foU/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/IcjPFAV1foU/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/IcjPFAV1foU/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/IcjPFAV1foU/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Bárbara Borges",
                "playlistId": "PL-QLQQumXurIwz9INwpGHj8g-2XOS1pjK",
                "position": 3,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": "IcjPFAV1foU"
                }
            }
        },
        {
            "kind": "youtube#playlistItem",
            "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/jYhzOcPRJnCV3xsuL9rAkTfehIY\"",
            "id": "UEwtUUxRUXVtWHVySXd6OUlOd3BHSGo4Zy0yWE9TMXBqSy41MzJCQjBCNDIyRkJDN0VD",
            "snippet": {
                "publishedAt": "2019-05-24T11:07:46.000Z",
                "channelId": "UCqMgknVzmr6jiW--D7aMIRQ",
                "title": "Peter Bjorn And John - Young Folks",
                "description": "Music video by Peter Bjorn And John performing Young Folks. (C) 2006 Universal Music AB\n\nhttp://vevo.ly/c1gGp0",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/iArXv64tCJA/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/iArXv64tCJA/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/iArXv64tCJA/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/iArXv64tCJA/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/iArXv64tCJA/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Bárbara Borges",
                "playlistId": "PL-QLQQumXurIwz9INwpGHj8g-2XOS1pjK",
                "position": 4,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": "iArXv64tCJA"
                }
            }
        }
    ]
};